package ru.maa.examples.ex1;

import java.util.Scanner;

/**
 * Created by User on 26.01.2017.
 */
public class Example1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите текст (и не забудьте поставить точку в конце) - ");
        String text = scanner.nextLine();
        int i = text.indexOf(".");
        System.out.println("Символов до точки: " + i);
        int p = text.indexOf(" ");
        System.out.println("Количество пробелов: " + p);
    }
}
