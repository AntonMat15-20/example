package ru.maa.examples.ex6;

import java.util.Scanner;

/**
 * Created by User on 26.01.2017.
 */
public class Example6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a;
        System.out.println("Введите число - ");
        a = scanner.nextInt();
        for (int i = 1; i <= 10; i++) {
            System.out.println(a + " * " + i + " = " + (a * i));
        }
    }
}
