package ru.maa.examples.ex8;

import java.util.Scanner;

/**
 * Created by User on 26.01.2017.
 */
public class Example8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double number;
        System.out.println("Введите число - ");
        number = scanner.nextDouble();
        System.out.println(number % 1 == 0 ? "Целое " : "Нецелое");

    }
}

