package ru.maa.examples.ex14;

import java.util.Scanner;

/**
 * Created by User on 26.01.2017.
 */
public class Example14 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите символ - ");
        String str = sc.next();
        char c = str.charAt(0);
        if (Character.isDigit(c)) System.out.println("Данный символ является цифрой!");
        if (Character.isLetter(c)) System.out.println("Данный символ является буквой!");
        if (".,:;?!".contains(str)) System.out.println("Данный символ является знаком пунктуации!");
    }
}
