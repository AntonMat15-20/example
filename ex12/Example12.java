package ru.maa.examples.ex12;

import java.util.Scanner;

/**
 * Created by User on 19.01.2017.
 */
public class Example12 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int minutes;
        int hour;
        int seconds;
        int days;
        System.out.println("Введите дни ");
        days = scanner.nextInt();
        hour = days * 24;
        System.out.println("Часов в " + days + " сутках = " + hour);
        minutes = 60 * hour;
        System.out.println("Минут в " + days + " сутках = " + minutes);
        seconds = minutes * 60;
        System.out.println("Секунд в " + days + " сутках = " + seconds);
    }
}
